extern crate rand;
#[macro_use]
extern crate clap;

use std::io::prelude::*;
use std::fs::File;
use std::path::Path;
use rand::Rng;
use clap::{App, Arg};

// struct to hold a ppm image
// assuming all images are type p6
struct PPMImage{
    width:  u32,
    height: u32,
    image:  Vec<u8>,
}

// implement functions for saving ppm images
impl PPMImage{
    fn new(width: u32, height: u32) -> PPMImage{
        // each pixel at x and y has 3 bytes allocated for it
        // we need height * width pixels
        let size = 3 * height * width;
        // cast size as usize since it's currently a u32
        // in order to do mass allocation you need a semicolon not a comma
        let image = vec![122; size as usize];
        PPMImage{ width: width, height: height, image: image}
    }

    // in order to change self.image i have to "borrow" self
    // as mutable to begin with
    fn set_pixel_color(&mut self, x:u32, y:u32, col:Color){
        // the 3 * x is for allocating the 3 bytes for RGB
        // y * self.width is to go to the correct row
        let index = 3 * (x + y * self.width);
        
        // index must be usize to index a vec correctly
        self.image[index as usize] = col.0;
        self.image[(index + 1) as usize] = col.1;
        self.image[(index + 2) as usize] = col.2
    }

    fn set_tile_color(&mut self, x_start:u32, y_start:u32,
                      tile_size:u32, col:Color){
        let mut x_end = x_start + tile_size;
        let mut y_end = y_start + tile_size;

        if x_end > self.width{
            x_end = self.width; 
        }
        if y_end > self.height{
            y_end = self.height;
        }

        for y in y_start..y_end{
            for x in x_start..x_end{
                self.set_pixel_color(x, y, col);
            }
        }
    }

    // i have to try! this 
    // 'self' is used so that it can be called from an instance
    fn write_file(&self, filename: &str) -> std::io::Result<()>{
        let path = Path::new(filename);
        // tryin to open the path created from 'filename'
        let mut file = try!(File::create(&path));
        // set the header
        let header = format!("P6 {} {} 255\n", self.width, self.height);
        // write the header
        try!(file.write(header.as_bytes()));
        // write the data from image
        try!(file.write(&self.image));
        // if it's the return line then you don't put the semicolon
        Ok(()) 
    }
}

#[derive(Copy, Clone)]
struct Color(u8,u8,u8);

#[allow(dead_code)]
enum Generator{
    TrueRandom,
    TiledRandom(Vec<Color>, Vec<u8>, u32),
}

// just testing first it won't return anything until i know
// how to call the other functions
#[allow(dead_code)]
fn generate_image(gen: Generator, x_size:u32, y_size:u32)
    -> PPMImage{
    match gen{
        Generator::TrueRandom => 
            generate_random_image(x_size, y_size), 
        Generator::TiledRandom(colors, probs, tiles) =>
            generate_tiled_image(x_size, y_size, tiles, &colors, &probs)
    }
}

// generates a truly random image, kind of useless but there
// if someone really wants to take their chances
fn generate_random_image(x_size:u32, y_size:u32) -> PPMImage{
    let mut image = PPMImage::new(x_size, y_size);
    let mut rng = rand::thread_rng();
    for y in 0..y_size{
        for x in 0..x_size{
            let rand_color = Color(rng.gen_range(0, 255),
                rng.gen_range(0, 255), rng.gen_range(0, 255));
            image.set_pixel_color(x, y, rand_color);
        }
    }
    image
}

#[allow(dead_code)]
// not truly random. It creates square tiles and has a set number of colors
// that can be used to color in the tiles
fn generate_tiled_image(x_size:u32, y_size:u32, tile_size:u32,
                        color:&Vec<Color>, prob:&Vec<u8>)
    -> PPMImage{

    let mut image = PPMImage::new(x_size, y_size);
    let mut x: u32 = 0;
    let mut y: u32 = 0;

    while y < y_size{
        // i needed to reset x back to 0 for this to work properly
        while x < x_size{
            let col_to_use = pick_random_color(color, prob);
            image.set_tile_color(x, y, tile_size, col_to_use);
            x += tile_size; 
        }
        y += tile_size;
        // gotta remember to start over
        x = 0;
    }

    image
}

// picks color from the colors vector using the probablity vector
fn pick_random_color(colors:&Vec<Color>, prob:&Vec<u8>)->Color{
    assert!(colors.len() == prob.len());

    let mut rand = rand::thread_rng();
    let mut i = 0;
    let mut cntr = 0;
    let mut color = colors[0];
    let rand_num = rand.gen_range(0, 100);

    // searching for the chosen color
    while i < colors.len(){
        if rand_num >= cntr && rand_num < cntr + prob[i]{
            color = colors[i]; 
        }
        cntr += prob[i];
        i += 1;
    }

    color
}


fn main(){
    // For the command line
    let matches = App::new("Rander Paper")
        // like -c some, and one positional argument
        .args(&[
            Arg::with_name("type")
                .help("Sets the type of file to use")
                .takes_value(true)
                .short("t")
                .long("type")
                .required(true),
            Arg::with_name("output")
                .help("Output file name for the generated image")
                .index(1)
                .required(true),
            Arg::with_name("resolution")
                .short("res")
                .takes_value(true)
                .long("resolution")
                .help("the resolution of the generated image width x height")
                .required(true)
                .multiple(true)
                .number_of_values(2)
        ])
        // convience methods for full functionality use from_usage
        // works just like with_name
        .arg_from_usage("--license 'display license file'")
        .get_matches();

    // this is required too but i don't feel like changing this
    if let Some(t) = matches.value_of("type"){
        // These are required so i can safely unwrap them
        let file = matches.value_of("output").unwrap();
        let res = matches.values_of("resolution").unwrap().collect::<Vec<_>>();
        // safe since 2 areguments are reqruired to even get to this part
        println!("The resolution of the image is {} {}", res[0], res[1]);

        match t.as_ref(){
            "RANDOM" => {
                println!("Generating random image: {}", file);
                let gen = Generator::TrueRandom;
                let image = generate_image(gen, 1920, 1080);
                image.write_file(file).unwrap();
            },
            "TILED"  =>{
                println!("Generating tiled image: {}", file);
                let gen = Generator::TiledRandom(vec![Color(249, 42, 204), Color(242, 12, 12),
                                                      Color(242, 19, 253)],
                                                 vec![33, 33, 34], 100);
                let image = generate_image(gen, 1920, 1080);
                image.write_file(file).unwrap();
            },
            _        => println!("The type of generator was not correct, try again.\nTODO print out correct thing"),
        }
    }
}
